﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class MoviesController : Controller
    {
        private ApplicationDbContext _dbContext;

        public MoviesController()
        {
            _dbContext = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _dbContext.Dispose();
        }

        public ActionResult Index()
        {
            var movieList = _dbContext.Movies.Include(c => c.Genre).ToList();

            return View(movieList);
        }

        public ActionResult New_Movie()
        {
            var genreList = _dbContext.Genres.ToList();

            var movieViewModel = new MovieFormViewModel()
            {
                Genre = genreList
            };

            return View("MovieForm", movieViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save_Movie(Movie movie)
         {
            if(!ModelState.IsValid)
            {
                var viewModel = new MovieFormViewModel(movie)
                {
                    Genre = _dbContext.Genres.ToList()
                };
                return View("MovieForm", viewModel);
            }

            if(movie.Id == 0)
            {
                movie.DateAdded = DateTime.Now.Date;
                _dbContext.Movies.Add(movie);
            }
            else
            {
                var movieInDb = _dbContext.Movies.Single(m => m.Id == movie.Id);

                movieInDb.Name = movie.Name;
                movieInDb.ReleaseDate = movie.ReleaseDate;
                movieInDb.GenreId = movie.GenreId;
                movieInDb.NumberInStock = movie.NumberInStock;
            }

            _dbContext.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Edit_Movie(int id)
        {
            var movie = _dbContext.Movies.SingleOrDefault(m => m.Id == id);

            if(movie == null)
            {
                return HttpNotFound();
            }

            var movieViewModel = new MovieFormViewModel(movie)
            {
                Genre = _dbContext.Genres.ToList()
            };

            return View("MovieForm", movieViewModel);
        }

        public ActionResult Details(int id)
        {
            var movie = _dbContext.Movies.Include(x => x.Genre).SingleOrDefault(x => x.Id == id);

            return View(movie);
        }

        public ActionResult Random()
        {
            var movie = new Movie() { Name = "Shrek" };

            var customers = new List<Customer>
            {
                new Customer { Name = "Customer1" },
                new Customer { Name = "Customer2" }
            };

            var viewModels = new RandomMovieViewModel
            {
                Movie = movie,
                Customers = customers
            };

            return View(viewModels);
        }

        //public ActionResult Index(int? pageIndex, string sortBy)
        //{
        //    var movie = new Movie() { Name = "Index" };

        //    if(!pageIndex.HasValue)
        //    {
        //        pageIndex = 1;
        //    }
        //    if(string.IsNullOrWhiteSpace(sortBy))
        //    {
        //        sortBy = "Name";
        //    }
        //    return Content(string.Format("Page Index = {0}, Sort By = {1}", pageIndex, sortBy));
        //}

        [Route("movies/released/{year}/{month:regex(\\d{2}):range(1,12)}")]
        public ActionResult ByReleaseDate(int year, int month)
        {

            return Content("Year =" + year + " && Month = " + month);
        }

        public ActionResult SampleAction(int id)
        {
            return Content(string.Format("{0}", id));
        }
    }
}
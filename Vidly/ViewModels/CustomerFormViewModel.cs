﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class CustomerFormViewModel
    {
        public IEnumerable<MembershipType> MembershipTypes { get; set; }

        public Customer Customers { get; set; }

        public string Title
        {
            get
            {
                return (Customers != null && Customers.Id != 0) ? "Edit Customer" : "Add Customer";
            }
        }
    }
}
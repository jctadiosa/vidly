﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationDbContext _dbContext;

        public CustomersController()
        {
            _dbContext = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _dbContext.Dispose();
        }

        // GET: Customers
        public ActionResult Index()
        {
            var customer = _dbContext.Customers.Include(c => c.MembershipType).ToList();

            return View(customer);
        }
        
        public ActionResult Details(int id)
        {
            var selectCustomer = _dbContext.Customers.Include(c => c.MembershipType).SingleOrDefault(x => x.Id == id);
            if(selectCustomer == null)
            {
                return HttpNotFound();
            }

            return View(selectCustomer);
        }

        public ActionResult New()
        {
            var membershipTypes = _dbContext.MembershipTypes.ToList();

            var customerViewModel = new CustomerFormViewModel()
            {
                Customers = new Customer(),
                MembershipTypes = membershipTypes
            };

            return View("CustomerForm", customerViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(CustomerFormViewModel customer_model)
        {
            if(!ModelState.IsValid)
            {
                var viewModel = new CustomerFormViewModel
                {
                    Customers = customer_model.Customers,
                    MembershipTypes = _dbContext.MembershipTypes.ToList()
                };
                return View("CustomerForm", viewModel);
            }

            if(customer_model.Customers.Id == 0)
            {
                _dbContext.Customers.Add(customer_model.Customers);
            }
            else
            {
                var customerInDB = _dbContext.Customers.Single(c => c.Id == customer_model.Customers.Id);

                customerInDB.Name = customer_model.Customers.Name;
                customerInDB.Birthdate = customer_model.Customers.Birthdate;
                customerInDB.MembershipTypeId = customer_model.Customers.MembershipTypeId;
                customerInDB.IsSubscribedToNewsLetter = customer_model.Customers.IsSubscribedToNewsLetter;
            }
            
            _dbContext.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var customer = _dbContext.Customers.SingleOrDefault(c => c.Id == id);

            if(customer == null)
            {
                return HttpNotFound();
            }

            var customerModel = new CustomerFormViewModel()
            {
                Customers = customer,
                MembershipTypes = _dbContext.MembershipTypes.ToList()
            };

            return View("CustomerForm", customerModel);
        }
    }
}
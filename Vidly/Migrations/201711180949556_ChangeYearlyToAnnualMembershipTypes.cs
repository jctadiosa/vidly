namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeYearlyToAnnualMembershipTypes : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE MembershipTypes SET Name = 'Annual' WHERE id = 4");
        }
        
        public override void Down()
        {
        }
    }
}
